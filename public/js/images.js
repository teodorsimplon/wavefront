window.onload = () => {
    //gestion des liend Supprimer
    let links = document.querySelectorAll("[data-delete]")
    // console.log(links)
    //on va boucle sur links

    for(link of links){
        // on ecoute le clic
        link.addEventListener("click", function(e){
            //on empeche la navigation en moment de click
            e.preventDefault()

            //on demand confirmation

            if(confirm("Voulez-vous supprimer cette image ?")){
                // on envoi une requete Ajax vers le href du lien avec la methode DELETE
                fetch(this.getAttribute("href"), {
                    method: "DELETE",
                    headers: {
                        "X-Requested-With": "XMLHttpRequest",
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({"_token":this.dataset.token})
                
                }).then(
                    // on recupere la reponse  en json
                    response => response.json()
                    

                ).then(data =>{

                    if(data.success)
                    this.parentElement.remove()
                    // else
                    //     alert(data.error)
                }).catch(e => alert(e));
            }

        })
    }
}