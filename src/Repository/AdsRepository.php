<?php

namespace App\Repository;

use App\Entity\Ads;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Ads|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ads|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ads[]    findAll()
 * @method Ads[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Ads::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Ads $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Ads $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
    // public function menuFindby(){
    //     $query = $this->createQueryBuilder('e')
    //     ->innerJoin('e.category_id','c')
    //     ->addSelect('c')
    //     ->orderBy('e.created_at', 'DESC')
    //     ->getQuery();
    //     return $query->getResult();
    // }
    // public function nameByCat(){
    //     return $this->findBy([], ['title' => 'search']);
    // }
    // /**
    //  * @return Ads[] Returns an array of Ads objects
    //  */
    
    // public function findbyCat($a, $b)
    // {
    //     return $this->createQueryBuilder('a')
    //         ->andWhere('a.category_id = b.id AND ')
    //         ->getQuery()
    //         ->getResult()
    //         ;
    // }
    

    /*
    public function findOneBySomeField($value): ?Ads
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
