<?php
namespace App\EventSubscriber;

use App\Entity\Users;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Easycorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class EasyAdminSubscriber implements EventSubscriberInterface
{
private $userPasswordHasher;

public function __construct(UserPasswordHasherInterface $userPasswordHasher)
{
    $this->userPasswordHasher = $userPasswordHasher;
}

public static function getSubscribedEvents(): array
{
    return [
        BeforeEntityUpdatedEvent::class => ['updatePassWord'],
        BeforeEntityPersistedEvent::class => ['hashPassWord'],
        // BeforeEntityUpdatedEvent::class => ['hashPassWord'],
    ];
}

public function hashPassWord(BeforeEntityPersistedEvent $event)
{
    $entity = $event->getEntityInstance();

    if (!($entity instanceof Users )) {
        return;
    }
    $entity->setPassword($this->userPasswordHasher->hashPassword($entity, $entity->getPassword()));
}

public function updatePassWord(BeforeEntityUpdatedEvent $event)
{
    $entity = $event->getEntityInstance();
    dd($entity);
    if (!($entity instanceof Users )) {
        return;
    }
    
    $entity->setPassword($this->userPasswordHasher->hashPassWord($entity, $entity->getPassword()));
}

}
