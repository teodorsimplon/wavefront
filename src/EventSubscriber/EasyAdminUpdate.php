<?php
namespace App\EventSubscriber;

use App\Entity\Users;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Easycorp\Bundle\EasyAdminBundle\Event\AfterEntityUpdatedEvent;
//use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class EasyAdminUpdate implements EventSubscriberInterface
{
private $userPasswordHasher;

public function __construct(UserPasswordHasherInterface $userPasswordHasher)
{
    $this->userPasswordHasher = $userPasswordHasher;
}

public static function getSubscribedEvents(): array
{
    return [
        // BeforeEntityUpdatedEvent::class => ['updatePassWord'],
        BeforeEntityPersistedEvent::class => ['hashPassWord'],
        // BeforeEntityUpdatedEvent::class => ['hashPassWord'],
    ];
}

// public function hashPassWord(BeforeEntityPersistedEvent $event)
// {
//     $entity = $event->getEntityInstance();

//     if (!($entity instanceof Users )) {
//         return;
//     }
//     $entity->setPassword($this->userPasswordHasher->hashPassword($entity, $entity->getPassword()));
// }

public function hashPassWord(AfterEntityUpdatedEvent $event)
{
    $entity = $event->getEntityInstance();
    dd($entity);
    if (!($entity instanceof Users )) {
        return;
    }
    
    $entity->setPassword($this->userPasswordHasher->hashPassWord($entity, $entity->getPassword()));
}

}
