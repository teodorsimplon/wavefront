<?php

namespace App\Controller;

use App\Entity\Users;
use App\Entity\Messages;
use App\Form\MessageType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MessageController extends AbstractController
{
    #[Route('/message', name: 'app_message')]
    public function index(): Response
    {
        return $this->render('message/index.html.twig', [
            'controller_name' => 'MessageController',
        ]);
    }

    #[Route('/send', name: 'app_send')]
    public function send(Request $request, EntityManagerInterface $entityManager, $id, ManagerRegistry $doctrine): Response
    {
        
        $message = new Messages();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $message->setSender($this->getUser());
            $entityManager->persist($message);
            $entityManager->flush();

            $this->addFlash("message", "Message envoyer avec succes");
            return $this->redirectToRoute("app_message");
        }

        return $this->render('message/send.html.twig',[
            "form" => $form->createView(),
        ]);
    }

    #[Route('/recus', name: 'app_recu')]
    public function received(): Response
    {
        return $this->render('message/recu.html.twig', [
        ]);
    }

    #[Route('/message/{id}{', name: 'app_message_read')]
    public function read(Messages $message, EntityManagerInterface $entityManager): Response
    {
        $message->setIsRead(true);
        $entityManager->persist($message);
        $entityManager->flush();

        return $this->render('message/read.html.twig', compact("message"));
    }

    #[Route('/delete/{id}{', name: 'app_message_delete')]
        public function delete(Messages $message, EntityManagerInterface $entityManager): Response
        {
            $entityManager->remove($message);
            $entityManager->flush();
    
            return $this->redirectToRoute("app_recu");
        }

    #[Route('/sended', name: 'app_sended')]
    public function sended(): Response
    {
        return $this->render('message/sender.html.twig', [
        ]);
    }

}
