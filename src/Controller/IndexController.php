<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Ads;
use App\Entity\Images;
use App\Entity\Categories;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;


class IndexController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    public function index(Request $request, ManagerRegistry $doctrine, EntityManagerInterface $entityManager): Response
    {
        $allCat = $doctrine->getRepository(Categories::class)->findAll();
        $category = $doctrine->getRepository(Categories::class);
        $ads = $doctrine->getRepository(Ads::class);
        if ($request->isMethod('POST')) {
            if($request->get('search') != '' && $request->get('category_select') != 'Sélectionnez une catégorie'){
                $selectCat = $category->findOneBy(['name' => $request->get('category_select')]);
                $researchedAds = $ads->findBy([
                    'title' => $request->get('search'),
                    'category_id' => $selectCat->getId(),
                ]);
            }
            else if ($request->get('search') == '' && $request->get('category_select') != 'Sélectionnez une catégorie'){
                $selectCat = $category->findOneBy(['name' => $request->get('category_select')]);
                $researchedAds = $ads->findBy(['category_id' => $selectCat->getId()]);
            }
            else {
            $researchedAds = $ads->findBy(['title' => $request->get('search')]);
        
            }
            return $this->render('index/index.html.twig', [
                'researchedAds' => $researchedAds,
                'categories' => $allCat,
                'ads' => [],
            ]);
        }
        $allAds = $ads->findBy(
            [],
            ['created_at' => 'DESC'],
            3,
        );
        return $this->render('index/index.html.twig', [
            'ads' => $allAds,
            'researchedAds' => [],
            'categories' => $allCat,
        ]);
    }
}

