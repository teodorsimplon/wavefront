<?php

namespace App\Controller\Admin;
use App\Entity\Ads;
use App\Entity\Users;
use App\Entity\Categories;
use App\Entity\SubCategories;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    public function __construct(private AdminUrlGenerator $adminUrlGenerator){

    }
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
       // return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
         $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
         //return $this->redirect($adminUrlGenerator->setController(UsersCrudController::class)->generateUrl());
        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
         if ($this->isGranted('ROLE_MODO')) {
             //dd($this->getUser()->$this->getRoles());
             return $this->redirect($adminUrlGenerator->setController(AdsCrudController::class)->generateUrl());
         }
         if ($this->isGranted('ROLE_ADMIN')) {
            return $this->redirect($adminUrlGenerator->setController(UsersCrudController::class)->generateUrl());
        }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from syAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Wavefront');
    }
    
    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::section('Utilisateurs')->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Ajouter un utilisateur', 'fas fa-plus', Users::class)->setAction(Crud::PAGE_NEW)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Voir les utilisateurs', 'fas fa-eye', Users::class)->setPermission('ROLE_ADMIN');

        yield MenuItem::section('Annonces');
        yield MenuItem::linkToCrud('Creer une annonce','fas fa-newspaper', Ads::class)->setAction(Crud::PAGE_NEW)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Voir les annonces', 'fas fa-eye', Ads::class);

        yield MenuItem::section('Categories')->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Ajouter une categorie','fas fa-plus', Categories::class)->setAction(Crud::PAGE_NEW)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Voir  les categories','fas fa-eye',Categories::class)->setPermission('ROLE_ADMIN');

        yield MenuItem::section('sous-categorie')->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Ajouter une sous-categorie','fas fa-plus', SubCategories::class)->setAction(Crud::PAGE_NEW)->setPermission('ROLE_ADMIN');
        yield MenuItem::linkToCrud('Voir  les sous-categories','fas fa-eye',SubCategories::class)->setPermission('ROLE_ADMIN');
    }
}
