<?php

namespace App\Controller\Admin;

use App\Entity\Ads;
use App\Entity\Users;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class AdsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Ads::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
        ->setPermission(Action::NEW,'ROLE_ADMIN')
        ;
    }
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('title','titre'),
            TextEditorField::new('content','description'),
            DateTimeField::new('created_at', 'date de création')->hideOnForm(),
            DateTimeField::new('updated_at', 'date de modification')->hideOnForm(),
            DateTimeField::new('deleted_at', 'date de suppression')->hideOnForm(),
            AssociationField::new('category_id','categorie'),
            AssociationField::new('user_id')->hideOnForm(),
            TextField::new('url_video','Lien video'),
            BooleanField::new('isVisible', 'Visible'),
            MoneyField::new('price','prix')->setCurrency('EUR'),
        ];
    }
    
    public function persistEntity(EntityManagerInterface $em, $entityInstance): void
    {
        if(!$entityInstance instanceof Ads) return;

        $entityInstance->setCreatedAt(new \DateTime());

        parent::persistEntity($em, $entityInstance);
    }
}
