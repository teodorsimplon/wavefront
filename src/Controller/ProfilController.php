<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\ProfilType;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class ProfilController extends AbstractController
{
    #[Route('/profil/{id}', name: 'app_profil')]
    public function index(Request $request, EntityManagerInterface $em,Users $users, UserPasswordHasherInterface $userPasswordHasher): Response
    {
        $form = $this->createForm(ProfilType::class, $users);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $users = $form->getData();
            $users->setPassword(
                $userPasswordHasher->hashPassword(
                    $users,
                    $form->get('password')->getData()
                )
        );
            $em->persist($users);
            $em->flush();

            $this->addFlash("message", "Profil mis a jour avec succés");
        }

        return $this->render('profil/index.html.twig', [
            'controller_name' => 'ProfilController',
            'form' => $form->createView(),

        ]);
    }
}
