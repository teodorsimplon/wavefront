<?php

namespace App\Controller;

use App\Entity\Ads;
use App\Entity\Users;
use App\Form\AdsType;
use App\Entity\Images;
use App\Entity\Categories;
use App\Repository\AdsRepository;
use Symfony\Component\Mime\Email;
use App\Repository\ImagesRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/ads')]
class AdsController extends AbstractController
{
    #[Route('/', name: 'app_ads_index', methods: ['GET'])]
    public function index(AdsRepository $adsRepository): Response
    {
        $user = $this->getUser();

        $ads = $adsRepository->findBy(['user_id' => $user]);

        return $this->render('ads/index.html.twig', [
            'ads' => $ads
        ]);
    }

    #[Route('/new', name: 'app_ads_new', methods: ['GET', 'POST'])]
    public function new(Request $request, AdsRepository $adsRepository,MailerInterface $mailer, ManagerRegistry $doctrine): Response
    {
        $ad = new Ads();

        $allCat = $doctrine->getRepository(Categories::class)->findAll();
        $usermodo = $doctrine->getRepository(Users::class)->findAll();

        $form = $this->createForm(AdsType::class, $ad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // on recupere les images transmises
            $images = $form->get('images')->getData();
            // on boucle sur les images
            foreach ($images as $image) {
                // on genere un nouveau fichier 
                $fichier = md5(uniqid()) . '.' . $image->guessExtension();
                // on copie le fichier dans le dossier uploads
                $image->move(
                    $this->getParameter('images_directory'),
                    $fichier

                );
                // on stocke l'image dans la base de donnees son nom
                $img = new Images();

                $img->setImageName($fichier);

                $ad->addImage($img);
            }
            $user = $this->getUser();
            $ad->setUserId($user);
            $cat = $doctrine->getRepository(Categories::class)->find($ad->getCategoryId());
            $ad->setCategoryId($cat);
            $ad->setIsVisible(true);

            $adsRepository->add($ad);
        
            foreach( $usermodo as $el)
            {
                $roles = $el->getRoles();
                $username = $user->getUserIdentifier();

               if(in_array("ROLE_MODO", $roles))
               {
                   $email = (new Email())
                    ->from('admin@example.com')
                    ->to($el->getEmail())
                    ->subject('nouvelle annonce')
                    ->text("Une nouvelle annonce vient d'etre publié par{ $username }");
                    $mailer->send($email);
               }

            }
            return $this->redirectToRoute('app_ads_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('ads/new.html.twig', [
            'ad' => $ad,
            'form' => $form,
            'categories' => $allCat,

        ]);
    }

    #[Route('/{id}', name: 'app_ads_show', methods: ['GET'])]
    public function show(Ads $ad): Response
    {
        return $this->render('ads/show.html.twig', [
            'ad' => $ad,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_ads_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Ads $ad, AdsRepository $adsRepository, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(AdsType::class, $ad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
          
            $images = $form->get('images')->getData();
            // on boucle sur les images
            foreach ($images as $image) {
                // on genere un nouveau fichier 
                $fichier = md5(uniqid()) . '.' . $image->guessExtension();
                // on copie le fichier dans le dossier uploads
                $image->move(
                    $this->getParameter('images_directory'),
                    $fichier

                );
                // on stocke l'image dans la base de donnees son nom
                
                $img = new Images();
                $img->setImageName($fichier);
                $ad->addImage($img);
            }
            $user = $this->getUser();
            $ad->setUserId($user);
            $cat = $doctrine->getRepository(Categories::class)->find($ad->getCategoryId());
            $ad->setCategoryId($cat);
            $ad->setIsVisible(true);

            $adsRepository->add($ad);
           
            
            return $this->redirectToRoute('app_ads_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('ads/edit.html.twig', [
            'ad' => $ad,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_ads_delete', methods: ['POST'])]
    public function delete(Request $request, Ads $ad, AdsRepository $adsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $ad->getId(), $request->request->get('_token'))) {
            $adsRepository->remove($ad);
        }

        return $this->redirectToRoute('app_ads_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/delete/image/{id}', name: 'app_image_delete', methods: ['DELETE'])]
    public function deleteImage(Images $image, Request $request, ImagesRepository $ImagesRepository)
    {
        $data = json_decode($request->getContent(), true);
        // on verifie si token est valide
        if ($this->isCsrfTokenValid('delete' . $image->getId(), $data['_token'])) {
            $nom = $image->getImageName();
            $ImagesRepository->remove($image);
            unlink($this->getParameter('images_directory') . '/' . $nom);

            //return $this->redirectToRoute('app_ads_index', [], Response::HTTP_SEE_OTHER);
            return new JsonResponse(['success' => 1]);
        } else {
            return new JsonResponse(['error' => 'Token Invalide'], 400);
        }
    }
    #[Route('/view/{id}', name: 'app_ads_view')]
    public function adsShow(ManagerRegistry $doctrine, $id)
    {
        $ads = $doctrine->getRepository(Ads::class)->findOneBy(['id' => $id]);
        return $this->render('ads/show_ads.html.twig', [
            'ads' => $ads,
        ]);
    }
}
