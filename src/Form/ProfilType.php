<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class ProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username',TextType::class,[
                'attr' => ['class' => 'form-control']
            ])

            ->add('email', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            
            ->add('password', PasswordType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'label' => 'Nouveau Mot de passe',
                    'placeholder' => 'Entre votre nouveau mot de passe'
                    ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
