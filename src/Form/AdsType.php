<?php

namespace App\Form;

use App\Entity\Ads;
use App\Entity\Categories;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AdsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title',TextType::class, [
                'label'=> 'Choisir le titre de votre annonce',
                "attr" => [
                    "class" => "form-control p-2 d-flex justify-content",
                ]
            ])
            ->add('content',TextareaType::class, [
                'label'=> 'Donnez une description a votre annonce',
                "attr" => [
                    "class" => "form-control p-2 d-flex justify-content",
                ]
            ])
            ->add('category_id', EntityType::class, [
                'label'=> 'Sélectionner la categorie dans laquelle vous voulez publier votre annonce',
                'class'=> Categories::class,
                'choice_label' => function($categories) {
                    return $categories-> getName();
                },
                'attr' => [
                    "class" => "form d-flex  p-2 justify-content-evenly",
                ]
            ])
            ->add('images', FileType::class, [
                'label' => 'Sélectionner les images sur votre ordinateur',
                'multiple' => true,
                'mapped' => false,
                'required' => false,
                "attr" => [
                    "class" => "form-control  p-2 d-flex justify-content",
                    "for"=> "formFileMultiple"
                ],
                //  'constraints' => [
                //      new Image([
                //          'maxSize' => '5M',
                //          'mimeTypes' => [
                //              'image/*',
                             
                //          ]
                //      ])
                //  ]
                // https://symfonycasts.com/screencast/symfony-uploads/validation 
         ])
          
            // ->add('url_video',TextType::class, [
            //     'required' => false,
            //     "attr" => [
            //         "class" => "form d-flex justify-content",
            //     ]
            // ])
           
            ->add('price',TextType::class, [
                "attr" => [
                    "class" => "form-control  p-2 d-flex justify-content",
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Ads::class,
        ]);
    }
}
