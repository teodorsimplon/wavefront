<?php

namespace App\Entity;

use App\Repository\CategoriesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoriesRepository::class)]
class Categories
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 45)]
    private $name;

    #[ORM\OneToMany(mappedBy: 'category_id', targetEntity: Ads::class, orphanRemoval: true)]
    private $ads;

    #[ORM\OneToMany(mappedBy: 'categories', targetEntity: SubCategories::class)]
    private $sub_cat;

    public function __construct()
    {
        $this->ads = new ArrayCollection();
        $this->sub_cat = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Ads>
     */
    public function getAds(): Collection
    {
        return $this->ads;
    }

    public function addAd(Ads $ad): self
    {
        if (!$this->ads->contains($ad)) {
            $this->ads[] = $ad;
            $ad->setCategoryId($this);
        }

        return $this;
    }

    public function removeAd(Ads $ad): self
    {
        if ($this->ads->removeElement($ad)) {
            // set the owning side to null (unless already changed)
            if ($ad->getCategoryId() === $this) {
                $ad->setCategoryId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SubCategories>
     */
    public function getSubCat(): Collection
    {
        return $this->sub_cat;
    }

    public function addSubCat(SubCategories $subCat): self
    {
        if (!$this->sub_cat->contains($subCat)) {
            $this->sub_cat[] = $subCat;
            $subCat->setCategories($this);
        }

        return $this;
    }

    public function removeSubCat(SubCategories $subCat): self
    {
        if ($this->sub_cat->removeElement($subCat)) {
            // set the owning side to null (unless already changed)
            if ($subCat->getCategories() === $this) {
                $subCat->setCategories(null);
            }
        }

        return $this;
    }

    public function  __toString()
    {
        return $this->name;
    }
}
